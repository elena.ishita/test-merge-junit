"""Utility functions."""

def add_numbers(a, b):
    """Adds two numbers."""
    return a + b

def subtract_numbers(a, b):
    """Sibtract two numbers."""
    return a - b

def multiply_numbers(a, b):
    """Multiply two numbers."""
    return a * b

def divide_numbers(a, b):
    """Divide two numbers."""
    return a / b

def square_number(a):
    """Square a number."""
    return a**2
