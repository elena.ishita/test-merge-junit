"""Utests."""

import sys
import unittest

sys.path.append('../..')
from my_module import utils as utils

class TestUtility(unittest.TestCase):
    """Perform unittests related to several utility functions."""

    def setUp(self):
        """Define data and setup environment."""
        self.a = 5
        self.b = 3

    def test_add(self):
        """Test addition."""
        self.assertEqual(utils.add_numbers(self.a, self.b), 8)

    def test_subtract(self):
        """Test additionion."""
        self.assertEqual(utils.subtract_numbers(self.a, self.b), 2)

    def test_multiply(self):
        """Test multiplication."""
        self.assertEqual(utils.multiply_numbers(self.a, self.b), 15)

    def test_divide(self):
        """Test division."""
        self.assertAlmostEqual(utils.divide_numbers(self.a, self.b), 1.6666666666666667)

    def test_square(self):
        """Test square."""
        self.assertEqual(utils.square_number(self.a), 25)
